#!/bin/sh

SIG=$(<sig)
DOMAIN="jihakuz.xyz"
TARGET_RECORDS="sonarr\nradarr\nserver"

echo "Waiting for ip..."

IP=$(nc -l -p 8082)

if [ $IP = $(<~/last_susan_ip) ] && exit

echo $IP > last_susan_ip

echo "Got ip: $IP."

echo "Getting records..."

records=$(curl -s -X GET "https://usersapiv2.epik.com/v2/domains/$DOMAIN/records?SIGNATURE=$SIG" -H "accept: application/json")

echo "Got records."

for record in $(echo $records | jq '.data.records' | jq -c '.[]');
do
	if grep -q "\"type\":\"A\"" <<< $record; then
		echo -e $TARGET_RECORDS |
		while IFS= read -r target_record; do
			if grep -q "$target_record" <<< $record; then
				id=$(echo $record | jq ".id" | sed "s/\"//g")
				res=$(curl -s -X PATCH "https://usersapiv2.epik.com/v2/domains/$DOMAIN/records?SIGNATURE=$SIG" -H  "accept: application/json" -H "Content-Type: application/json" -d "{\"update_host_records_payload\": { \"ID\": \"$id\", \"HOST\": \"$target_record\", \"TYPE\": \"A\", \"DATA\": \"$IP\", \"AUX\": 0, \"TTL\": 30 }}")
				echo "Response: $res"
			fi
		done
	fi
done
